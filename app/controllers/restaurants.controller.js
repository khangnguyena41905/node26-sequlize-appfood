const { response } = require("../helpers/response");
const restaurantServs = require("../services/restaurants.service");

const getRestaurants = () => {
  return async (req, res, next) => {
    try {
      const restaurants = await restaurantServs.getRestaurants();
      res.status(200).json(response(restaurants));
    } catch (error) {
      next(error);
    }
  };
};

const createRestaurant = () => {
  return async (req, res, next) => {
    try {
      const restaurantData = req.body;
      const newRestaurant = await restaurantServs.createRestaurant(
        restaurantData
      );
      res.status(200).json(response(newRestaurant));
    } catch (error) {
      next(error);
    }
  };
};

const likeRestaurant = () => {
  return async (req, res, next) => {
    try {
      const { restaurantId } = req.params;
      const { userId } = req.body;
      await restaurantServs.likeRestaurant(userId, restaurantId);
      res.status(200).json(response("Ok"));
    } catch (error) {
      next(error);
    }
  };
};

const rateRestaurant = () => {
  return async (req, res, next) => {
    try {
      const { restaurantId } = req.params;
      const { userId } = req.body;
      await restaurantServs.rateRestaurant(userId, restaurantId);
      res.status(200).json(response("Ok"));
    } catch (error) {
      next(error);
    }
  };
};

module.exports = {
  getRestaurants,
  createRestaurant,
  likeRestaurant,
  rateRestaurant,
};
