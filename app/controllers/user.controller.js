const { response } = require("../helpers/response");
const userServs = require("../services/users.service");

const getUsers = () => {
  return async (req, res, next) => {
    try {
      const users = await userServs.getUsers();
      res.status(200).json(response(users));
    } catch (error) {
      next(error);
    }
  };
};

const getUserById = () => {
  return async (req, res, next) => {
    try {
      const { userId } = req.params;
      const user = await userServs.getUserById(userId);
      res.status(200).json(response(user));
    } catch (error) {
      next(error);
    }
  };
};

const createUser = () => {
  return async (req, res, next) => {
    try {
      const dataUser = req.body;
      const newUser = await userServs.createUser(dataUser);
      res.status(200).json(response(newUser));
    } catch (error) {
      next(error);
    }
  };
};

const deleteUser = () => {
  return async (req, res, next) => {
    try {
      const { userId } = req.params;
      await userServs.deleteUser(userId);
      res.status(200).json(response("OK"));
    } catch (error) {
      next(error);
    }
  };
};

module.exports = { getUsers, getUserById, createUser, deleteUser };
