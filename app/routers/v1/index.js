const express = require("express");
const {
  createRestaurant,
  getRestaurants,
  likeRestaurant,
  rateRestaurant,
} = require("../../controllers/restaurants.controller");
const {
  getUsers,
  getUserById,
  createUser,
  deleteUser,
} = require("../../controllers/user.controller");

//  path v1: /api/v1
const v1 = express.Router();

// Users
v1.get("/users", getUsers());
v1.get("/users/:userId", getUserById());
v1.post("/users", createUser());
v1.delete("/users/:userId", deleteUser());
module.exports = v1;

// Restaurant
v1.get("/restaurants", getRestaurants());
v1.post("/restaurants", createRestaurant());
v1.put("/restaurants/likes/:restaurantId", likeRestaurant());
v1.put("/restaurants/rates/:restaurantId", rateRestaurant());
