const response = (payload, ...rest) => {
  return {
    status: "succes",
    data: payload,
    ...rest,
  };
};
module.exports = { response };
