const { Sequelize } = require("sequelize");

const sequelize = new Sequelize("db_food", "root", "1234", {
  dialect: "mysql",
  host: "localhost",
  port: 3306,
});

(async () => {
  try {
    await sequelize.authenticate();
    console.log("Connection has been established successfully.");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
})();

const User = require("./User")(sequelize);
const Restaurant = require("./Restaurant")(sequelize);
const LikeRestaurants = require("./LikeRestaurants")(sequelize);
const RateRestaurants = require("./RateRestaurants")(sequelize);

// relationship
// Like restaurants
User.belongsToMany(Restaurant, {
  as: "restaurantLikes",
  through: LikeRestaurants,
  foreignKey: "userId",
});
Restaurant.belongsToMany(User, {
  as: "userLikes",
  through: LikeRestaurants,
  foreignKey: "restaurantId",
});
// Rate restaurant
User.belongsToMany(Restaurant, {
  as: "restaurantRates",
  through: RateRestaurants,
  foreignKey: "userId",
});
Restaurant.belongsToMany(User, {
  as: "userRates",
  through: RateRestaurants,
  foreignKey: "restaurantId",
});
module.exports = { sequelize, User, Restaurant };
