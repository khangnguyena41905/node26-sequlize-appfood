const { AppError } = require("../helpers/error");
const { Restaurant, User } = require("../models");
const RateRestaurants = require("../models/RateRestaurants");

const getRestaurants = async () => {
  try {
    const restaurants = await Restaurant.findAll({
      include: [
        {
          association: "userLikes",
          attributes: {
            exclude: ["id", "password"],
          },
          through: {
            attributes: [],
          },
        },
        {
          association: "userRates",
          attributes: {
            exclude: ["id", "password"],
          },
          through: {
            attributes: [],
          },
        },
      ],
    });
    return restaurants;
  } catch (error) {
    throw new AppError(400, "Có gì đó sai sai với DB!!!");
  }
};

const createRestaurant = async (data) => {
  try {
    const newRestaurant = await Restaurant.create(data);
    return newRestaurant;
  } catch (error) {
    throw new AppError(400, "Sao không tạo được nhà hàng ta???");
  }
};

const likeRestaurant = async (userId, restaurantId) => {
  try {
    const restaurant = await Restaurant.findByPk(restaurantId);
    if (!restaurant) {
      throw new AppError(400, "Nhà hàng này không tồn tại");
    }

    const user = await User.findByPk(userId);
    if (!user) {
      throw new AppError(400, "Người dùng không tồn tại");
    }

    // console.log(restaurant.__proto__); tìm kiếm các func của restaurant
    const hasLiked = await restaurant.hasUserLike(user.id);

    if (hasLiked) {
      await restaurant.removeUserLike(user.id);
    } else {
      await restaurant.addUserLike(user.id);
    }

    return null;
  } catch (error) {
    console.log("error: ", error);
  }
};

const rateRestaurant = async (userId, restaurantId) => {
  try {
    const restaurant = await Restaurant.findByPk(restaurantId);
    if (!restaurant) {
      throw new AppError(400, "Nhà hàng này không tồn tại");
    }

    const user = await User.findByPk(userId);
    if (!user) {
      throw new AppError(400, "Người dùng không tồn tại");
    }

    // console.log(restaurant.__proto__); tìm kiếm các func của restaurant
    const hasRated = await restaurant.hasUserRate(user.id);

    if (hasRated) {
      await restaurant.removeUserRate(user.id);
    } else {
      await restaurant.addUserRate(user.id);
    }

    return null;
  } catch (error) {
    console.log("error: ", error);
  }
};

module.exports = {
  getRestaurants,
  createRestaurant,
  likeRestaurant,
  rateRestaurant,
};
