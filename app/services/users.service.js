const { AppError } = require("../helpers/error");
const { User } = require("../models");

const getUsers = async () => {
  try {
    const users = await User.findAll({
      include: [
        {
          association: "restaurantLikes",
          attributes: {
            exclude: ["id", "password"],
          },
          through: {
            attributes: [],
          },
        },
        {
          association: "restaurantRates",
          attributes: {
            exclude: ["id", "password"],
          },
          through: {
            attributes: [],
          },
        },
      ],
    });
    return users;
  } catch (error) {
    throw new AppError(500, "Hình như có cái gì nó sai sai ở DB :))))");
  }
};

const getUserById = async (userId) => {
  try {
    const user = await User.findOne({
      where: {
        id: userId,
      },
    });
    if (!user) {
      throw new AppError(400, "Hình như không có người này ?????");
    }
    return user;
  } catch (error) {
    throw error;
  }
};

const createUser = async (dataUser) => {
  try {
    const isEmailExist = await User.findOne({
      where: {
        email: dataUser.email,
      },
    });

    if (isEmailExist) {
      throw new AppError(400, "Email này có người tạo rồi brooo!!!");
    }

    if (!dataUser.password) {
      dataUser.password = Math.random().toString(36).substring(2);
      // Gửi email về cho user mật khẩu nàyk
    }

    const newUser = await User.create(dataUser);

    return newUser;
  } catch (error) {
    throw error;
  }
};

const deleteUser = async (userId) => {
  try {
    const user = User.findOne({
      where: {
        id: userId,
      },
    });
    if (!user) {
      throw new AppError(400, "Người dùng không tồn tại");
    }
    await User.destroy({ where: { id: userId } });
    return null;
  } catch (error) {
    throw error;
  }
};

module.exports = { getUsers, getUserById, createUser, deleteUser };
